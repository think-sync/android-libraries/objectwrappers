package com.thinknsync.objectwrappers;

public interface DialogWrapper<T> extends FrameworkWrapper<T>{
    void showDialog(String... parameters);
    void hideDialog();
}
