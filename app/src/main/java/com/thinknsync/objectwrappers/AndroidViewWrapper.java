package com.thinknsync.objectwrappers;

import android.view.View;

public class AndroidViewWrapper extends BaseFrameworkWrapper<View>{

    public static final String tag = "view";

    public AndroidViewWrapper(View view) {
        setFrameworkObject(view);
    }
}
