package com.thinknsync.objectwrappers;

import androidx.fragment.app.Fragment;

public class FragmentWrapper extends BaseFrameworkWrapper<Fragment> {
    public static final String tag = "fragment";

    public FragmentWrapper(Fragment fragment) {
        setFrameworkObject(fragment);
    }
}
