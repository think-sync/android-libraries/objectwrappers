package com.thinknsync.objectwrappers;

public interface FrameworkWrapper<T> {
    T getFrameworkObject();
    void setFrameworkObject(T frameworkObject);
}
