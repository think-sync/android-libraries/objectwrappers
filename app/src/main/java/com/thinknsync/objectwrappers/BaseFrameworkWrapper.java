package com.thinknsync.objectwrappers;

public abstract class BaseFrameworkWrapper<T> implements FrameworkWrapper<T> {
    
    private T frameworkObject;
    
    @Override
    public T getFrameworkObject(){
        return frameworkObject;
    }

    @Override
    public void setFrameworkObject(T frameworkObject){
        this.frameworkObject = frameworkObject;
    }
}
