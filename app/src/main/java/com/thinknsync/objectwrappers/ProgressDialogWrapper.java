package com.thinknsync.objectwrappers;

public interface ProgressDialogWrapper<T> extends FrameworkWrapper<T>{
    String tag = "progressDialogWrapper";

    void showProgress();
    void hideProgress();
    void hideProgressWithDelay(int delay);
    void setProgressValue(int value);
    void setCancelable(boolean cancelable);
    void setMessage(String message);
}
