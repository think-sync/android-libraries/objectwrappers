package com.thinknsync.objectwrappers;

import android.app.AlertDialog;
import android.content.DialogInterface;

/**
 * Created by shuaib on 8/23/16.
 */

public class AlertDialogWrapper extends BaseFrameworkWrapper<AlertDialog>
        implements DialogWrapper<AlertDialog> {

    private AndroidContextWrapper contextWrapper;
    private boolean dialogueShown;

    public AlertDialogWrapper(){}

    public AlertDialogWrapper(AndroidContextWrapper contextContextWrapper){
        this.contextWrapper = contextContextWrapper;
    }

    @Override
    public void showDialog(String... parameters) {
        AlertDialog.Builder builder = new AlertDialog.Builder(contextWrapper.getFrameworkObject());
        if(parameters.length > 0) {
            builder.setTitle(parameters[0])
                    .setMessage(parameters[1])
                    .setCancelable(false)
                    .setNegativeButton(parameters[2], new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            hideDialog();
                        }
                    });
        } else {
            builder.setNegativeButton("Ok", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    hideDialog();
                }
            });
        }
        setFrameworkObject(builder.create());
        getFrameworkObject().show();
        dialogueShown = true;
    }

    @Override
    public void hideDialog() {
        if(dialogueShown){
            getFrameworkObject().cancel();
            dialogueShown = false;
        }
    }
}
