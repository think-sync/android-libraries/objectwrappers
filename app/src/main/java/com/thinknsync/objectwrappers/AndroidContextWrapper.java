package com.thinknsync.objectwrappers;

import android.content.Context;

public class AndroidContextWrapper extends BaseFrameworkWrapper<Context>{

    public static final String tag = "context";

    public AndroidContextWrapper(Context context) {
        setFrameworkObject(context);
    }
}
