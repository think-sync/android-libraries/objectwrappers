package com.thinknsync.objectwrappers;

import android.os.Bundle;

public class BundleWrapper extends BaseFrameworkWrapper<Bundle> {
    public static final String tag = "bundle";

    public BundleWrapper(Bundle bundle) {
        setFrameworkObject(bundle);
    }
}
