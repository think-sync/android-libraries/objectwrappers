package com.thinknsync.objectwrappers;

import android.app.PendingIntent;

public class PendingIntentWrapper extends BaseFrameworkWrapper<PendingIntent> {

    public PendingIntentWrapper(PendingIntent pendingIntent) {
        setFrameworkObject(pendingIntent);
    }
}
