package com.thinknsync.objectwrappers;

import androidx.appcompat.app.AppCompatActivity;

public class AndroidActivityWrapper<s extends AppCompatActivity> extends BaseFrameworkWrapper<s> implements ObjectValidator{

    public static final String tag = "activity";

    public AndroidActivityWrapper(s activity) {
        setFrameworkObject(activity);
    }

    @Override
    public void validateObject(Object object) {

    }
}
