package com.thinknsync.objectwrappers;

public interface ToastWrapper {
    void showShortToast(String message);
    void showLongToast(String message);
    void showToast(String message, int duration);
}
