package com.thinknsync.objectwrappers;

/**
 * Created by shuaib on 10/16/16.
 */

public class UndefinedObjectException extends Exception {

    private final String DEFAULT_MESSAGE = "Object type is not defined for api. Define new object type in class";

    public UndefinedObjectException() {
        super();
        new UndefinedObjectException(DEFAULT_MESSAGE);
    }

    public UndefinedObjectException(String message) {
        super(message);
    }

    public UndefinedObjectException(String message, Throwable cause) {
        super(message, cause);
    }

    public UndefinedObjectException(Throwable cause) {
        super(cause);
    }
}
