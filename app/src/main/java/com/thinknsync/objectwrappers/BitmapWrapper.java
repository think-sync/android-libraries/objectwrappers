package com.thinknsync.objectwrappers;

import android.graphics.Bitmap;

public class BitmapWrapper extends BaseFrameworkWrapper<Bitmap> {
    public static final String tag = "bitmap";

    public BitmapWrapper(Bitmap bitmap) {
        setFrameworkObject(bitmap);
    }
}
