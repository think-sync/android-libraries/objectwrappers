package com.thinknsync.objectwrappers;

import android.app.PendingIntent;
import android.content.Context;
import android.widget.Toast;

/**
 * Created by shuaib on 6/21/17.
 */

public class ToastWrapperImpl extends BaseFrameworkWrapper<Toast> implements ToastWrapper{

    private AndroidContextWrapper contextWrapper;
    private int duration_short = Toast.LENGTH_SHORT;
    private int duration_long = Toast.LENGTH_LONG;

    public ToastWrapperImpl(AndroidContextWrapper contextWrapper){
        this.contextWrapper = contextWrapper;
    }

    @Override
    public void showShortToast(String message) {
        Toast.makeText(contextWrapper.getFrameworkObject(), message, duration_short).show();
    }

    @Override
    public void showLongToast(String message) {
        Toast.makeText(contextWrapper.getFrameworkObject(), message, duration_long).show();
    }

    @Override
    public void showToast(String message, int duration) {
        Toast.makeText(contextWrapper.getFrameworkObject(), message, duration).show();
    }
}
