package com.thinknsync.objectwrappers;

import android.app.ProgressDialog;
import android.os.Handler;

/**
 * Created by shuaib on 8/23/16.
 */

public class ProgressDialogWrapperImpl extends BaseFrameworkWrapper<ProgressDialog>
        implements ProgressDialogWrapper<ProgressDialog>{

    public ProgressDialogWrapperImpl(final ProgressDialog progressDialog){
        setFrameworkObject(progressDialog);
    }

    @Override
    public void showProgress() {
        if(getFrameworkObject() != null) {
            getFrameworkObject().show();
        }
    }

    @Override
    public void hideProgress() {
        if(getFrameworkObject() != null){
            getFrameworkObject().dismiss();
        }
    }

    @Override
    public void hideProgressWithDelay(int delay) {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                hideProgress();
            }
        }, delay);
    }

    @Override
    public void setProgressValue(int value){
        if(getFrameworkObject() != null) {
            getFrameworkObject().setProgress(value);
        }
    }

    @Override
    public void setCancelable(boolean cancelable) {
        if(getFrameworkObject() != null) {
            getFrameworkObject().setCancelable(cancelable);
        }
    }

    @Override
    public void setMessage(String message) {
        if(getFrameworkObject() != null) {
            getFrameworkObject().setMessage(message);
        }
    }
}
