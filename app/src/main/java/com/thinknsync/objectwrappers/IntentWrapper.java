package com.thinknsync.objectwrappers;

import android.content.Intent;

public class IntentWrapper extends BaseFrameworkWrapper<Intent> {
    public static final String tag = "intent";

    public IntentWrapper(Intent intent) {
        setFrameworkObject(intent);
    }
}
