package com.thinknsync.objectwrappers;

/**
 * Created by shuaib on 5/13/17.
 */

public class InvalidWrapperArgumentException extends Exception {

    private final String DEFAULT_MESSAGE = "Object type is not valid for farmework object wrapper.";

    public InvalidWrapperArgumentException() {
        super();
        new UndefinedObjectException(DEFAULT_MESSAGE);
    }

    public InvalidWrapperArgumentException(String message) {
        super(message);
    }

    public InvalidWrapperArgumentException(String message, Throwable cause) {
        super(message, cause);
    }

    public InvalidWrapperArgumentException(Throwable cause) {
        super(cause);
    }
}
